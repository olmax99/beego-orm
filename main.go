package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/beego/beego/v2/client/orm"

	"github.com/beego/beego/v2/core/config"
	"github.com/goombaio/namegenerator"

	_ "github.com/mattn/go-sqlite3"
)

func init() {
	// Create DB
	os.MkdirAll("./data/dev/", 0755)
	os.Create("./data/dev/onc.db")

	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "./data/dev/default.db")
	orm.RegisterDataBase("oncdb", "sqlite3", "./data/dev/onc.db")
}

func main() {

	iniconf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		fmt.Println(err)
	}

	db_boot, err := iniconf.String("db::beego_db_bootstrap")
	if err != nil {
		fmt.Println(err)
	}
	db_debug, err := iniconf.String("db::beego_db_debug")
	if err != nil {
		fmt.Println(err)
	}

	// Load configs
	if db_debug == "true" {
		orm.Debug = true
	}

	// orm.RunSyncdb needs to run
	if db_boot == "true" {
		name := "oncdb"
		force := true
		verbose := true
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		name := "oncdb"
		force := false
		verbose := false
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	}

	// Enable cmd: orm syncdb
	orm.RunCommand()

	fillTable(30)

}

func fillTable(num int) {
	answers := []string{
		"It is certain",
		"It is decidedly so",
		"Without a doubt",
		"Yes definitely",
		"You may rely on it",
		"As I see it yes",
		"Most likely",
		"Outlook good",
		"Yes",
		"Signs point to yes",
		"Reply hazy try again",
		"Ask again later",
		"Better not tell you now",
		"Cannot predict now",
		"Concentrate and ask again",
		"Don't count on it",
		"My reply is no",
		"My sources say no",
		"Outlook not so good",
		"Very doubtful",
	}
	emails := []string{
		"yahoo.com",
		"gmail.com",
		"yahoo.com",
		"hotmail.com",
		"aol.com",
		"hotmail.co.uk",
		"hotmail.fr",
		"msn.com",
		"yahoo.fr",
		"wanadoo.fr",
		"orange.fr",
		"comcast.net",
		"yahoo.co.uk",
		"yahoo.com.br",
		"yahoo.co.in",
		"live.com",
		"rediffmail.com",
		"free.fr",
		"gmx.de",
	}
	for i := 0; i < num; i++ {
		// Using default, you can use other database
		o := orm.NewOrmUsingDB("oncdb")

		seed := time.Now().UTC().UnixNano()
		min := 10
		max := 80
		age := int16(rand.Intn(max-min) + min)
		create := randate()

		profile := new(Profile)
		profile.Age = age
		profile.Answer = answers[rand.Intn(len(answers))]
		profile.Created = create

		nameGenerator := namegenerator.NewNameGenerator(seed)
		name := nameGenerator.Generate()

		user := new(User)
		user.Profile = profile
		user.Name = name
		user.Email = name + "@" + emails[rand.Intn(len(emails))]

		o.Insert(profile)
		o.Insert(user)
	}

}

// return a random time.Time within a pre-defined period
func randate() time.Time {
	min := time.Date(2015, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2021, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}
